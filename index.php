<?php
date_default_timezone_set('Europe/London');
$time_start = microtime(true);
if(!defined('BASEDIR'))
{
    define('BASEDIR', realpath(dirname(__FILE__)));
}
if(!defined('ENV'))
{
    define('ENV', 'development');
}

require_once BASEDIR . '/inc/Library/Doggy/Application.php';
$application = new Doggy_Application();
$application->run();

$time_end = microtime(true) - $time_start;
echo '<!-- ' . $time_end . '-->';