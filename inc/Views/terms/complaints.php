 <div class="grid_22 push_1 main">
<h1>Complaints</h1>
<p>This notice sets out details of the complaints procedure which we operate to try to resolve complaints which you, the client, may have with regard to the services that we provide to you. This complaints procedure is aimed at resolving complaints quickly and satisfactorily and further improving the quality of our service.</p>

<p><strong>What our complaints procedure covers</strong><br />
Our complaints procedure covers complaints which you may wish to make with regard to the services which we have provided to you, in particular, the manner in which we have dealt with your Creditors and the information we have provided to you about our dealings on your behalf.</p>

<p><strong>How to make a complaint</strong><br />
If you are unhappy with the service we have provided to you, you should in the first instance discuss the situation with your Initial Adviser or Personal Finance Manager. Should you be dissatisfied with the explanation he or she has provided to you then your complaint will be referred to the company's Compliance Department. Written complaints should be addressed to the Compliance Department.</p>

<p><strong>How we will action your complaint</strong><br />
The Compliance Department will consider the contents of your complaint. We may need to contact you for further information in order to better understand your position. We will conduct a full investigation into the points raised and discuss the details of your complaint with your Initial Adviser and/or Personal Finance Manager as appropriate. Once all information has been considered you will be contacted further to confirm whether the company accepts either completely or to some degree your complaint. Where applicable, you will be advised of what steps the company will take to put right the complaint and ensure that the same problem does not reoccur. It is the company's objective to resolve a complaint satisfactorily within 10 working days although where more detailed investigation is required the process may take longer. You will be kept informed of the time scale that will be required for us to investigate your complaint.</p>

<p><strong>Financial Ombudsman Service</strong><br />
If you are not satisfied with our final response, you may be eligible to refer the matter to the Financial Ombudsman Service, details of which will be provided at that time.</p>

<p><strong>Your Rights</strong><br />
We hope that you will accept the decision of our Compliance Department. If this should not be the case, you remain free at all times to seek an independent form of advice.</p>
</div>