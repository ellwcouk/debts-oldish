 <div class="grid_7 sitemap push_4">
    <h3>Debt Management</h3>
    <ul>
    	<li><a href="<?php echo $this->url('debt-management'); ?>" title="Debt Mangement">Debt Management</a></li>
        <li><a href="<?php echo $this->url('debt-management', 'how-a-debt-management-plan-works'); ?>" title="How a Debt Management Plan Works">How a debt management plan works</a></li>
        <li><a href="<?php echo $this->url('debt-management', 'debt-management-plan'); ?>" title="Debt Management Plan DMP">Debt Management Plan (DMP)</a></li>
   </ul>
   </div>
   <div class="grid_7 sitemap push_4">
   	<h3>Other Solutions</h3>
    <ul>
    	<li><a href="<?php echo $this->url('other-solutions'); ?>" title="Other Solutions">Other Solutions</a></li>
        <li><a href="<?php echo $this->url('other-solutions', 'scottish-trust-deed'); ?>" title="Scottish Trust Deed">Scottish Trust Deed</a></li>
        <li><a href="<?php echo $this->url('other-solutions', 'bankruptcy'); ?>" title="bankcruptcy">Bankruptcy</a></li>
        <li><a href="<?php echo $this->url('other-solutions', 'individual-voluntary-arrangement'); ?>" title="Individual Voluntary Arrangement (IVA)">Individual Voluntary Arrangement (IVA)</a></li>
        <li><a href="<?php echo $this->url('other-solutions', 're-mortgaging'); ?>" title="Re-Mortgaging or Mortgages">Re-Mortgaging</a></li>
    </ul>
   </div>
   <div class="clear"></div>
   <div class="grid_22 sitemap center">
   	<h1>Rest of Site</h1>
   </div>
   <div class="grid_7 push_4 sitemap">
   	<ul>
    	<li><a href="<?php echo $this->url('index'); ?>" title="Home">Home</a></li>
        <li><a href="<?php echo $this->url('get-help-now'); ?>" title="Get Help Now">Get Help Now</a></li>
        <li><a href="<?php echo $this->url('faq'); ?>" title="Frequently Asked Questions FAQ">FAQ</a></li>
        <li><a href="<?php echo $this->url('documents'); ?>" title="Documents">Documents</a></li>
    </ul>
   </div>
   <div class="grid_7 push_4 sitemap">
   	<ul>
        <li><a href="<?php echo $this->url('terms', 'privacy-policy'); ?>" title="Privacy Policy">Privacy Policy</a></li>
        <li><a href="<?php echo $this->url('terms', 'complaints'); ?>" title="Complaints">Complaints</a></li>
        <!--<li><a href="/?&c=switchtomobile" title="Switch to Mobile Site">Mobile Site</a></li>-->
        <li><a href="<?php echo $this->url('sitemap.xml'); ?>" title="Sitemap">Sitemap XML</a></li>
    </ul>
   </div>
