<div class="grid_22 push_1">
	<h1>Documents</h1>
    <p>Please find below a list of important documents for our clients that are on Debt Management Plans.</p>

	<p>To view these documents you will need to use Acrobat Reader*</p>
  <p align="center"><em><a href="http://get.adobe.com/uk/reader/?promoid=BUIGO" target="_blank"><img src="<?php echo $this->baseUrl('img/adobe.png'); ?>" alt="Download Acrobat Reader" width="158" height="60" /></a> </em></p>

	<div class="grid_2 center push_7">
    <a href="<?php echo $this->url('documents', 'download', array('file' => 'applicationform', 'version' => '545')); ?>"><img src="<?php echo $this->baseUrl('img/reader.jpg'); ?>" /></a>
    Application Form
    </div>
    <div class="grid_2 center push_7">
    <a href="<?php echo $this->url('documents', 'download', array('file' => 'termsandconditions', 'version' => '545')); ?>"><img src="<?php echo $this->baseUrl('img/reader.jpg'); ?>" /></a>
    Terms of Business
    </div>
    <div class="grid_2 center push_7">
    <a href="<?php echo $this->url('documents', 'download', array('file' => 'standingorder', 'version' => '545')); ?>"><img src="<?php echo $this->baseUrl('img/reader.jpg'); ?>" /></a>
    Standing Order Form
    </div>
    <div class="grid_2 center push_7">
    <a href="<?php echo $this->url('documents', 'download', array('file' => 'clientauthority', 'version' => '545')); ?>"><img src="<?php echo $this->baseUrl('img/reader.jpg'); ?>" /></a>
    Client Authority
    </div>
    <div class="clear"></div>
    <p>&nbsp;</p>
  <p style="font-size:10px;">* Please be advised that Debts Reduced Ltd is not responsible for any information contained within the Adobe Acrobat Reader website, nor is Debts Reduced Ltd responsible for any personal, account, or other information you may be asked to provide while visiting the Adobe Acrobat Reader website. </p>
</div>
