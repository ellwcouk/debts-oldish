<div class="grid_22 push_1 main">
	<h1>Debt Management</h1>
    <h4 style="margin-top:-25px;margin-left: 10px;">What are my options?</h4>
    <div class="option">
    	<h6><a href="<?php echo $this->url('debt-management', 'debt-management-plan'); ?>" title="Debt Management Plan (DMP)">Debt Management Plan (DMP)</a></h6>
        <p>Debt management reduces the amount you pay to your unsecured debts each month, making your other costs easier to manage. It simplifies your finances and gives you all the help you need to take control of your debts.<br />
            <a href="<?php echo $this->url('debt-management', 'debt-management-plan'); ?>" title="Debt Management Plan (DMP)">Read More</a></p>
    </div>
    <div class="option">
    	<h6><a href="<?php echo $this->url('other-solutions'); ?>" title="Other Solutions">Other Solutions</a></h6>
        <p>See our other solutions. These may suit you needs more.<br />
        <a href="<?php echo $this->url('other-solutions'); ?>" title="Other Solutions">Read More</a></p>
    </div>
    <div class="option toolbg">
    	<h6><a href="<?php echo $this->url('debt-management', 'find-a-solution'); ?>" title="Find a Solution">Find a Solution</a></h6>
        <p>Use our online solution finder to find what solutions are available to you.<br />Please remember that our debt solution finder is just an information tool.<br />
            <a href="<?php echo $this->url('debt-management', 'find-a-solution'); ?>" title="Find a Solution">Find a Solution</a></p>
    </div>
</div>
