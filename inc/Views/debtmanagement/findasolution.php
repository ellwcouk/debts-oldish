<div class="push_1 grid_22">
<div class="grid_14 push_3 bluebox">
	<div id="1">
    <h6>What is your approximate debt?</h6>
  	<div class="slider" style="margin-left: 70px;"></div> 
        <span class="sliderresult">£<span id="slider-result"><?php echo Doggy_Layout::getInstance()->v; ?></span>,000</span>
  	<input type="hidden" id="hidden"/>
    </div>
    <div class="hidden" id="2">
    <h6>Some small details</h6>
    <form id="form1" name="form1" method="post" action="">
	<table width="400" border="0" class="niceform biggerform" style="margin-left: 80px;">
	  <tr>
	    <td>
	      <select name="wheredoyoulive" id="wheredoyoulive">
	        <option value="0">Where Do You Live?</option>
	        <option value="England">England</option>
	        <option value="Wales">Wales</option>
	        <option value="Scotland">Scotland</option>
	        <option value="Northen Ireland">Northen Irelend</option>
	        </select></td>
	    <td>
	      <input type="text" name="name" id="name" value="First Name" class="empty" /></td>
      </tr>
	  <tr>
	    <td><select name="homeowner" id="homeowner">
	      <option value="0">Are you a homeowner?</option>
	      <option value="Yes">Yes</option>
	      <option value="No">No</option>
	      </select></td>
	    <td>
	      <input type="text" name="lastname" id="lastname" class="empty" value="Last Name" /></td>
      </tr>
	  <tr>
	    <td><select name="employment" id="employment">
	      <option value="0">Your Employment Status</option>
          <option value="Employed full time">Employed full-time</option>
<option value="Employed part time">Employed part-time</option>
<option value="Self employed">Self employed</option>
<option value="Unemployed">Unemployed</option>
<option value="Retired">Retired</option>
<option value="Student">Student</option>
	      </select></td>
	    <td><input name="telephone" type="text" id="telephone" value="Telephone Number" class="empty" /></td>
      </tr>
	  <tr>
	    <td>
	      <input type="checkbox" name="privacypolicy2" id="privacypolicy2" />Agree to the <a href="<?php echo $this->baseUrl('terms/privacy-policy'); ?>" target="_blank">Privacy Policy</a></td>
	    <td>&nbsp;</td>
      </tr>
    </table>
    </form>
</div>
    <div class="next"><a href="<?php echo $this->baseUrl('debt-management/find-a-solution'); ?>" alt="Next step" id="sliderresulta" alt="Next step"><img src="<?php echo $this->baseUrl('img/next.png'); ?>" width="53" height="48" alt="Next" /></a></div>
</div>
<div class="clear"></div>
<br />
<h1>How do I use the Debt Solution Tool?</h1>
<p id="whattodo">Slide the silver icon across until the number matches your approximate debt.</p>
</div>
<div class="clear"></div>