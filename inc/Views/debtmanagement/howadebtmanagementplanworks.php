<div class="grid_15 push_1 main">
	<h1>How a debt management plan works</h1>
    <div style="font-size: 25px;">
    	<img src="<?php echo $this->baseUrl('img/person.png'); ?>" /> Good for you
    </div>
    <ul class="tick">
    	<li>A monthly payment you can afford</li>
        <li>A simpler way of clearing your debts</li>
        <li>No more calls or letters from lenders</li>
        <li>A way to gain control of your finances</li>
        <li>Access to expert debt advice</li>
    </ul>
    
    <div style="font-size: 25px;">
    	<img src="<?php echo $this->baseUrl('img/company.png'); ?>" /> Good for your lenders
    </div>
    <ul class="tick">
    	<li>Reliable monthly debt repayments</li>
        <li>Professional company to work with</li>
        <li>The knowledge that your repaying your debts as fast as you can</li>
        <li>Higher chance of getting all the monies paid</li>
    </ul>
</div>
<div class="grid_5">
	<img src="<?php echo $this->baseUrl('img/pie.jpg'); ?>" />
</div>
<div class="clear"></div>
<div class="grid_5 push_1">
	<img src="<?php echo $this->baseUrl('img/feet.jpg'); ?>" />
</div>
<div class="grid_14 main">
<h1>We take easy, small steps</h1>
<ul class="tick">
	<li>We make sure debt management is right for you
    	<ul>
        	<li>Our expert advisers will discuss your situation to get a clear understanding of your financial position. We'll need to know all of your financial commitments and how much money you have coming in and going out. This information is helf in the strictest confidence at all times.</li>
        </ul>
    </li>
    <li style="margin-top: 80px;">We find the best solution for you
    	<ul>
        	<li>From the information you have given us, we can calculate how much you can pay each month. If a different plan (i.e. an IVA) is more suitable, we will tell you and arrange this for you.</li>
        </ul>
    </li>
    <li style="margin-top:40px;">We take on all communication with your lenders
    	<ul>
        	<li>Once your happy with your debt management plan, we'll contact your lenders on behalf to ask them to direct all correspondence and any questions to us. You shouldn't be bothered by them again</li>
        </ul>
    </li>
    <li style="margin-top:60px;">You make one affordable monthly payment to us
    	<ul>
        	<li>Once your debt management plan is agreed, we'll handle all your lender payments and communications on your behalf.</li>
        </ul>
    </li>
</ul>
<p>&nbsp;</p>
<h2><a href="<?php echo $this->url('debt-management', 'debt-management-plan'); ?>" title="Debt Management Plan (DMP)">Make life more affordable, now!</a></h2>
<div class="clear"></div>
<br />
</div>