  <div class="grid_22 push_1">
	<h1>Debt Management Plan (DMP)</h1>
    <p class="slogan">Is a Debt Mangement Plan (DMP) Right for me?</p>
    <p>A typical Debt Management Plan (DMP) will consolidate all your unsecured debts into a single and more affordable monthly repayment. This should not be confused with the more formal and more conclusive process of an&nbsp;<a href="<?php echo $this->url('other-solutions', 'individual-voluntary-arrangement'); ?>" title="Individual Voluntary Arrangement">Individual Voluntary Arrangement (IVA)</a>. </p>
 <p>In a Debt Management Plan (DMP), your debts are repaid to creditors on a pro-rata basis over an agreed period of time. This payment is determined by the amount you can reasonably afford after your normal cost of living has been deducted from your income, and will be presented to your creditors by a debt management company. Creditors are not obliged to agree to the Debt Management Plan, but they are more likely to accept it once they have been made properly aware of your true position.</p>
 <p>Once your creditors have agreed to a DMP, it is your responsibility to continue paying the agreed payments to their administrator. The administrator (normally the debt management company who drafted the plan) will then ensure that these payments are distributed promptly, on a pro rata basis, to all creditors until the plan has been successful completed.</p>
 <p>Each DMP should be regularly reviewed, especially to consider whether creditors would accept early settlement. Once a DMP is completed, you will be considered debt  and able to make a fresh financial start.</p>
 <p>Before you take out a DMP, make sure that you use a fully licensed debt management company that adheres to OFT debt management guidelines, Debts Reduced is such a company. Establish any charges that you will have to pay for their services, and ask to see evidence that they are trying to.</p>
 <p>You will pay to us your first two monthly payments on the dates  specified. <strong><u>This will be our programme set up fee and not paid to  creditors.</u></strong><br />
We  have a separate account into which we will pay the monthly payment amount. This  is for the sole purpose of making payments to creditors in accordance with the  programme. This account is called the Client Account. Payments are made to  creditors 5 days after funds are cleared.&nbsp;  We shall be entitled to pay the monthly management fee from your monthly  payments prior to payment being made to your creditors. We shall then make  payment to creditors in accordance with your programme provided that we have  sufficient funds from you. </p>
 <p><strong>Each month we will charge you 17.5% of the agreed payment  amount or a minimum of &pound;35.00. All fees charged are VAT exempt. </strong></p>
<p><strong>Please note that a Debt Management Plan may:</strong></p>
 <ul>
   <li>   Lead to increase in the sum to be repaid</li>
   <li>Lengthen the repayment period</li>
   <li> Additional fees or interest maybe charged by your creditors</li>
   <li> Your credit rating will be affected for up to 6 years, if not already so.</li>
 </ul>
<p>&nbsp;</p>
<h2><a href="<?php echo $this->url('get-help-now'); ?>" title="Get Help Now">Get Help Now!</a></h2>
</div>
