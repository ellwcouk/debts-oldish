<div class="grid_22 push_1 main">
	<h1>Other Solutions</h1>
    <h4 style="margin-top:-25px;margin-left: 10px;">What else can I do?</h4>
    <div class="option">
    	<h6><a href="<?php echo $this->url('other-solutions', 'scottish-trust-deed'); ?>" title="Scottish Trust Deed">Scottish Trust Deed</a></h6>
        <p>If you live in Scotland, at the end of the agreement, any unsecured debt you can't afford to repay will be written off.<br />
            <a href="<?php echo $this->url('other-solutions', 'scottish-trust-deed'); ?>" title="Scottish Trust Deed">Read More</a></p>
    </div>
    <div class="option">
        <h6><a href="<?php echo $this->url('other-solutions', 'individual-voluntary-arrangement'); ?>" title="Individual Voluntary Arrangement (IVA)">Individual Voluntary Arrangements (IVAs)</a></h6>
        <p>An IVA makes unmanageable debts affordable again. By reducing your unsecured debt repayments to an affordable level and combining them into one monthly payment.<br />
            <a href="<?php echo $this->url('other-solutions', 'individual-voluntary-arrangement'); ?>" title="Individual Voluntary Arrangement (IVA)">Read More</a></p>
    </div>
    <div class="option">
    	<h6><a href="<?php echo $this->url('other-solutions', 'bankruptcy'); ?>" title="Bankruptcy">Bankruptcy</a></h6>
        <p>Bankruptcy writes off some of the unsecured debt you can't afford. Bankruptcy should only be considered as a last resort.<br />
        <a href="<?php echo $this->url('other-solutions', 'bankruptcy'); ?>" title="Bankruptcy">Read More</a></p>
    </div>
    <div class="option">
    	<h6><a href="<?php echo $this->url('other-solutions', 're-mortgaging'); ?>" title="Re-mortgaging">Re-Mortgaging</a></h6>
        <p>Re-Mortgaging can help clear unsecured debt by borrowing money against your home.<br />
        <a href="<?php echo $this->url('other-solutions', 're-mortgaging'); ?>" title="Re-mortgaging">Read More</a></p>
    </div>
    
    <div class="option toolbg">
    	<h6><a href="<?php echo $this->url('debt-management', 'find-a-solution'); ?>" title="Find a Solution">Find a Solution</a></h6>
        <p>Use our online solution finder to find what solutions are available to you.<br />Please remember that our debt solution finder is just an information tool.<br />
            <a href="<?php echo $this->url('debt-management', 'find-a-solution'); ?>" title="Find a Solution">Find a Solution</a></p>
    </div>
</div>
