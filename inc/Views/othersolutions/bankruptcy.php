 <div class="grid_22 push_1">
	<h1>Bankruptcy</h1>
    <p class="slogan">What is bankruptcy?</p>
    <p>You should consider bankruptcy if you have substantial debt that you will never realistically be able to clear. In most cases, bankruptcy should be a last resort, as you will face stigma, possible job loss, liquidated assets including your property, loss of office and a chronic credit record.</p>
    <p>&nbsp;</p>
    <p class="slogan">Is bankruptcy right for me?</p>
    <ul>
   <li>Yes, if you have considered all the alternatives and decided you still have no realistic chance of clearing your debt.</li>
   <li>No, if you have not considered the&nbsp;<a href="<?php echo $this->url('other-solutions'); ?>" title="Other Debt Solutions">other options</a>&nbsp;for repaying your debts.</li>
 </ul>
 <p>As a last resort, you can petition for your own bankruptcy at your local county court. The cost for a personal bankruptcy petition is &pound;485. If you receive means tested benefits, you may be exempt or able to pay a reduced fee.</p>
 <p>Normally people would only voluntarily declare themselves bankrupt if they had few or no assets to protect. There are certain assets that are excluded from bankruptcy, such as personal possessions, household furniture, clothing, tools of the trade and certain pension entitlements.</p>
 <p>If you have assets that are not excluded (such as equity in your property) then these will need to be realised and paid into your bankruptcy estate. If your assets need to be sold, this will be handled by an Insolvency Practitioner, who is appointed as the trustee in bankruptcy. Any assets you acquire during your bankruptcy will become the property of the trustee.</p>
 <p>Depending on your earnings, your trustee may require you to make contributions to your creditors out of your income. The amount that is paid to creditors is determined by the amount that you can reasonably afford after your normal cost of living expenses have been deducted from your income. This is called an income payments order (IPO), and it will remain in force for three years from the date of bankruptcy. This means that even though you may have been discharged from your bankruptcy after one year, you will still have to continue paying your IPO for a further two years.</p>
 <p>If you have had a bankruptcy petition filed against you, you should seek professional advice as soon as possible. If you have been made bankrupt recently, you may still be able to have the bankruptcy annulled in favour of an&nbsp;<a href="<?php echo $this->url('other-solutions', 'individual-voluntary-arrangement'); ?>" title="Individual Voluntary Arrangement">Individual Voluntary Arrangement (IVA)</a>.</p>
 <p>&nbsp;</p>
<h2><a href="<?php echo $this->url('get-help-now'); ?>" title="Get Help Now">Get Help Now!</a></h2>
 </div>
