            <div class="grid_22 push_1">
<h1>Re-Mortgaging</h1>
<p class="slogan">What is re-mortgaging?</p>
<p>Re-mortgaging can clear unsecured debt by borrowing money against your home</p><p>&nbsp;</p>
<p class="slogan">Is re-mortgaging right for me?</p>
<p>There are many reputable lenders who will lend up to 65% of the open market value of your property, but this decreases depending on your credit rating. Generally speaking, the more adverse credit you have (such as mortgage arrears, CCJs or default notices) the lower the percentage will be. The interest rates you will have to pay will also depend on your credit rating.</p>
  <p>If you do choose to re-mortgage your property you should ensure that the lender you use is properly regulated by the Financial Services Authority (FSA).</p>
  <p>We are able to introduce you to some of the largest financial institutions in the UK, who will advise you on the best deals available for your particular circumstances. They are obliged to be completely transparent about their rates of interest, their charges and any penalties that may apply for early settlement.</p>
  <p>You should also ensure that you would be able to afford the monthly repayments. There is no sense in converting your unsecured debt into a secured debt, only to find that you cannot maintain the payments. This could become extremely serious, as your home will now be at risk. You should also take into consideration that interest rates could easily rise.</p>
 <p>&nbsp;</p>
<h2><a href="<?php echo $this->url('get-help-now'); ?>" title="Get Help Now">Get Help Now!</a></h2>
</div>