<div class="grid_22 push_1">
	<h1>Individual Voluntary Arrangement (IVA)</h1>
    <p class="slogan">What is an Individual Voluntary Arrangement (IVA)?</p>
    <p>Below is a list of questions which most clients ask when dealing with us. If your question you want answered is not in this list, do not hesitate to contact us and we will gladly answer it for you.</p>

<p>An <strong>Individual  
                              Voluntary Arrangement </strong>- IVA is a formal 
                              agreement between you and your creditors where you 
                              will come to an arrangement with people you owe 
                              money to, to make reduced payments towards the 
                              total amount of your debt in order to pay off a 
                              percentage of what you owe then generally after 5 - 6
                              years your debt is classed as settled.&nbsp; Due 
                              to its formal nature, an Individual Voluntary 
                              Arrangement - IVA has to be set up by a licensed 
                              professional called an Insolvency 
                              Practioner (IP).</p>
                              <Pt><STRONG>Its Purpose?</STRONG><br />It 
                              is a legally binding agreement between you and 
                              your creditors (people you owe money to). It helps 
                              those in financial difficulties to make a formal 
                              proposal to settle their debt. </P>
                              
                              <P><STRONG>What Are The 
                              Arrangements?</STRONG><br />Monthly payments are 
                              based on an affordable disposable income. Once the 
                              final payment is made, any outstanding debt is 
                              legally written off. <br /><br /><strong>How does it work? 
                              </strong><br />
                              Debts are settled within a reasonable and 
                              fixed period of time (normally 5 - 6 years). Any 
                              interest and debt charges will be frozen and 
                              creditors will be prohibited from demanding 
                              additional payments. <br /><br />
                              Once a decision has 
                              been made that an Individual Voluntary Arrangement 
                              - IVA is right for you, you will be asked 
                              questions regarding your current financial 
                              situation. Based on the information you have 
                              given, a repayment amount will be agreed with you. 
                              Once proposals have been drawn up you will need to 
                              check and sign these and return them to your Insolvency 
                              Practitioner (IP). <br />
                              <br />
                              An application may 
                              then be made to the court for an Interim 
                              Order. Once this is in place, no creditors 
                              will be able to take legal action against you. You 
                              may be asked to attend your creditors meeting but 
                              this rarely happens, normally you are asked to be 
                              contactable by phone on the day. <br />
                              <br />For an 
                              Individual Voluntary Arrangement - IVA to be 
                              approved, creditors will be called upon to vote 
                              either for or against the arrangement. If only one 
                              creditor votes "for" the Individual Voluntary 
                              Arrangement - IVA, the Individual Voluntary 
                              Arrangement - IVA will be approved. However, if 
                              only one creditor votes against the Individual 
                              Voluntary Arrangement - IVA and they represent 
                              less than 25% of your total debt, the meeting will 
                              be suspended for a later date and other creditors 
                              who did not vote will be called upon for their 
                              vote. <br /><br />If the creditor who voted against 
                              the Individual Voluntary Arrangement - IVA 
                              represents more than 25% of the total debt you owe 
                              the Individual Voluntary Arrangement - IVA will 
                              fail. This is because an Individual Voluntary 
                              Arrangement - IVA will only ever be approved if 
                              75% in monetary value is voted for. If any of the 
                              creditors don't vote, it is assumed that they will 
                              vote FOR the Individual Voluntary Arrangement - 
                              IVA. <br /><br />The Individual Voluntary Arrangement 
                              - IVA will be legally binding. As long as you keep 
                              up the repayments, when the term of your agreement 
                              is finished, you will be  from these debts 
                              regardless of how much has been paid off. 
                              <br /><br />During the period of your arrangement your 
                              financial situation will be reviewed regularly to 
                              see if there has been any change in your 
                              circumstances. <br /><br />It is very important that 
                              consumers do not confuse an Individual Voluntary 
                              Arrangement - IVA with a Debt Management Plan, 
                              which are not legally binding. <br /><br />Most 
                              Individual Voluntary Arrangement - IVA cases are 
                              based around one, affordable, monthly, payment, 
                              over a period of 60 months. This one affordable 
                              payment is based on your earnings minus your 
                              expenses.<br /><br />An Individual Voluntary 
                              Arrangement - IVA proposal has to be prepared by a 
                              licensed Insolvency Practitioner (IP) who then 
                              presents it to creditors at a creditors meeting. 
         <h3>Debt Advice - Call now on 0844 335 1355</h3>
          In the case of a consumer Individual 
                              Voluntary Arrangement - IVA it is unusual for any 
                              creditors or their representatives to attend the 
                              creditors meeting as most prefer to vote by fax or 
                              by post. <br /><br />The rules of an Individual 
                              Voluntary Arrangement - IVA state that providing 
                              75% (in value terms) of those that have voted, 
                              vote to accept the proposals (with or without 
                              modifications) then the Individual Voluntary 
                              Arrangement - IVA is agreed and becomes legally 
                              binding on all other parties whether they voted or 
                              not. <br /><br />When an Individual Voluntary 
                              Arrangement - IVA is accepted the IP's role 
                              becomes that of supervisor, monitoring the 
                              Individual Voluntary Arrangement - IVA's progress 
                              and ensuring that the terms and conditions that 
                              were agreed to at the creditors meeting are 
                              properly adhered to. <br /><br />It is the debtor's 
                              responsibility to pay the agreed payments to the 
                              IP who will then ensure that these payments are 
                              distributed to all creditors on a pro-rata basis 
                              in accordance with terms and until the successful 
                              completion of the Individual Voluntary Arrangement 
                              - IVA. It is in the debtors own interest to 
                              maintain their payments as failure to pay will 
                              almost certainly result in the failure of the 
                              Individual Voluntary Arrangement - IVA. 
                              <br /><br />Upon the successful completion of the 
                              Individual Voluntary Arrangement - IVA the debtor 
                              will be considered debt  even though they may 
                              not have actually paid off all of their debts in 
                              full. Any outstanding balances are written off 
                              (known as a composition of debts) and the debtor 
                              is then  to make a fresh financial start. 
                              <br /><br />It is worth noting that if you do enter 
                              into an Individual Voluntary Arrangement - IVA 
                              with your creditors and you have an endowment 
                              policy linked to your mortgage then you may be 
                              expected to cash it in and pay the proceeds into 
                              the arrangement. Likewise, if your property has a 
                              reasonable amount of equity then it is likely that 
                              a some of it will have to be released at sometime 
                              during the arrangement (usually the end), so it 
                              can be paid to creditors. Drastic as this may 
                              sound it can be a deciding factor in whether an 
                              Individual Voluntary Arrangement - IVA is approved 
                              by creditors and a realistic way in which a debtor 
                              can retain their property. <br /><br /><B>Why Avoid 
                              Other Debt Solutions?</STRONG><br /></B>There are 
                              many organisations who advertise debt solutions. 
                              Their biggest claim is to be able to reduce 
                              monthly loan and credit card payments to a single 
                              affordable amount. Such reductions are often 
                              possible to achieve and can provide peace of mind. 
                              However, these agreements are normally informal 
                              “gentleman’s agreements” and are not legally 
                              binding.<br />&nbsp;</P>
                              <P><STRONG>What Are The Hidden Problems with DMP? 
                              </STRONG></P>
                              <UL>
                                <LI>Although they may greatly reduce your 
                                repayments each month, you will still have to 
                                pay all of your debt back. This could take a 
                                very long time. Example: A debt of £20,000 with 
                                reduced monthly repayments of £200 a month will 
                                leave you repaying your debt for at least 9 
                                years. 
                                <LI>Your creditors do not have to stop adding 
                                interest and late payment charges. Some may for 
                                a short period (perhaps 6 months). 
                                <LI>Creditors may want to review the situation. 
                                This means that the reduced amount paid each 
                                month may only just cover the extra interest 
                                being added. If this is the case, then the debt 
                                will never be repaid. 
                                <LI>Your creditors can break the agreement at 
                                any time and asked for increased payments or add 
                                further interest. 
                                <LI>Not knowing where you stand with creditors, 
                                may have you always waiting for that next 
                                payment demand letter to come through the post. 
                                </LI></UL>
                              <P><STRONG>What are the costs of an IVA?</STRONG></P>
                              <P> Typically once enough monthly payments have been made after the arrangement has been approved, a fee of &pound;1,500-2,500 + VAT will be drawn for the initial work in preparing and circulating the proposal and holding the creditors' meeting depending on the complexity of the case.&nbsp; Then once per year an annual supervision charge of typically 15% + VAT of all realisations going in to the IVA.&nbsp; The annual charge covers the cost of monitoring the monthly payments, general correspondence, and sending a report and dividend to the creditors once per year.&nbsp; Where two partners propose two IVAs in respect of joint household debts the initial cost is likely to be &pound;3,000 + VAT with again the annual cost being set at 15% of realisations. </P>
                              <P><STRONG>                              Why use an IVA? 
                              <br />
                              </STRONG>Besides overcoming the problems 
                              outlined in Why avoid other Debt Solutions? An IVA 
                              offers a real solution, a “Light at the end of the 
                              Tunnel”, where informal debt solutions as 
                              described above do not.</P>
                              <UL>
                                <LI>You have an agreement with your creditors to 
                                make a single reduced payment each month. </LI>
                                <LI>It lasts for a sensible period of time (normally 5 years)<strong>.</strong></LI>
                                <LI>Once agreed, creditors are not allowed to 
                                add further interest or charges to your accounts 
                                by law. </LI>
                                <LI>The agreement is fixed, meaning that 
                                creditors cannot randomly demand changes to it. </LI>
                                
</UL>

  <p ><strong>Will I be able to get a mortgage or further   loans/finance/effect on credit rating?</strong></p>
  <p> This will greatly depend on your credit status, which <strong>will be</strong> adversely affected by your participation in any IVA. <strong>The affect on your credit rating will last for 6 years.</strong></p>
  <p><strong>What is the effect of IVA failure or an IVA failing?</strong>                                    </p>
  <p>We receive a lot of calls and emails from the public asking; &quot;<em>What is the effect of IVA failure or an IVA failing?</em>&quot;.</p>
                                    <p>If the IVA falls so much into arrears that nothing can be done to bring it   back on track then the IVA Supervisor will have no choice but to issue a Notice   of Termination. This will formally bring an end to the IVA. </p>
                                    <p>If the IVA fails then the Creditors will be able to chase you for the   remaining debt, as you will no longer have the protection afforded to you by the   IVA. </p>
                                    <p>There is also the risk that Creditors or the Insolvency Practitioner could   petition for your Bankruptcy.</p>
 <p>&nbsp;</p>
<h2><a href="<?php echo $this->url('get-help-now'); ?>" title="Get Help Now">Get Help Now!</a></h2>
</div>