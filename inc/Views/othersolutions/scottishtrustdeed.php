<div class="grid_22 push_1">
	<h1>Scottish Trust Deed</h1>
    <p class="slogan">What is a Scottish Trust Deed?</p>
    <p>A Scottish Trust Deed gives you the opportunity to make a formal proposal to your creditors in order to clear your debt. This should not be confused with the less formal and less conclusive <a href="<?php echo $this->url('debt-management', 'debt-management-plan'); ?>" title=" Debt Management Plan">Debt Management Plan (DMP)</a>.</p>
    <p>&nbsp;</p>
    <p class="slogan">Is a Scottish Trust Deed right for me?</p>
    <ul>
    <li>Yes, if you live in Scotland and are struggling with high levels of debt.</li>
    <li>No, if you are in debt with loans secured against your property. Take a look at the&nbsp;<a href="<?php echo $this->url('other-solutions'); ?>" title="Other Debt Solutions">other options</a>.</li>
  </ul>
  <p>A typical Trust Deed would consolidate all of your unsecured debts into a single and more affordable monthly repayment over a period of 36 months, which is then paid to creditors on a pro-rata basis. The amount payable to creditors is determined by the amount you can reasonably afford to pay after your normal cost of living has been deducted from your income, taking into account your assets and liabilities. This will ensure that you don&rsquo;t get into arrears or miss paying any of your priority commitments.</p>
  <p>In order to enter into a Trust Deed, your proposal is drafted with the help of a licensed Insolvency Practitioner, known as a trustee. The trustee acts as an honest broker between you and your creditors to ensure that the proposal being drafted is realistic and fair to everyone involved. When the proposal is finished, a draft copy is sent to you for approval.</p>
  <p>All creditors are informed about the proposals being made and if no creditors with claims of more than a third of the total debts vote to reject the proposals, the Trust Deed becomes protected and therefore legally binding on all of the creditors.</p>
  <p>When a Trust Deed is &lsquo;protected', creditors must stop any further interest from accruing on their outstanding debts. The trustee becomes a supervisor, monitoring the Trust Deed&rsquo;s progress and making sure that its terms and conditions are followed properly. Providing you keep to these terms, your debts will be considered settled in full when the 36 months are over. Any outstanding balances are written off, and you can make a fresh financial start.</p>
<p>&nbsp;</p>
<h2><a href="<?php echo $this->url('get-help-now'); ?>" title="Get Help Now">Get Help Now!</a></h2>
</div>
