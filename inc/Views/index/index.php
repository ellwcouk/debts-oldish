<br />
<div class="grid_9 push_1">
    <img src="<?php echo $this->baseUrl('img/man.png'); ?>" alt="Get rid of your stress with Debts Reduced" />
</div>
<div class="grid_15">
	<h1 style="font-size: 35px;color: #228EAB;"><img src="<?php echo $this->baseUrl('img/arrow.png'); ?>" alt="Arrow" />Find my best Debt Solution</h1>
</div>
<div class="grid_14 bluebox">
    <h6>What is your approximate debt?</h6>
    <div class="slider" style="margin-left: 70px;"></div> 
    <span class="sliderresult">£<span id="slider-result">5</span>,000</span>
    <input type="hidden" id="hidden"/>
    <div class="next"><a href="<?php echo $this->url('debt-management', 'find-a-solution', array('v' => '5')); ?>" id="sliderresulta"><img src="<?php echo $this->baseUrl('img/next.png'); ?>" width="53" height="48" alt="Next" /></a></div>
</div>
<div class="clear"></div>
<p>&nbsp;</p>
<div class="grid_6 push_1 blueborder justify">
    <h3>See how debt management works</h3>
    <p>See how a debt management plan could help reduced your monthly repayments and stop creditors calling you.</p>
    <a href="<?php echo $this->url('debt-management', 'how-a-debt-management-plan-works'); ?>" class="readmore">
        <img src="<?php echo $this->baseUrl('img/pig.png'); ?>" alt="See how a debt management plan works" style="margin-top: -10px;margin-left: 5px;" />
        <img src="<?php echo $this->baseUrl('img/readmore.png'); ?>" alt="Read More" width="150" height="31" /></a>
</div>
<div class="grid_10 push_1 blueborder">
	<h3>Debt Management Help</h3>
    <div class="grid_2">
    	<img src="<?php echo $this->baseUrl('img/cog.jpg'); ?>" alt="Cog" />
    </div>
    <div class="grid_8" style="margin-left:0;margin-right: 0;">
    	<a class="debtmanhelp" href="<?php echo $this->url('debt-management', 'how-a-debt-management-plan-works'); ?>">How a Debt Management Plan (DMP) works</a><br />
        Understand how a debt management plan<br />works to reduce your payments
    </div>
    <div class="grid_2" style="margin-top: 5px;">
    	<img src="<?php echo $this->baseUrl('img/faq.png'); ?>" alt="FAQ"/>
    </div>
    <div class="grid_8" style="margin-left:0;margin-right: 0;margin-top:5px;">
    	<a href="<?php echo $this->url('faq'); ?>" class="debtmanhelp">Debt Management FAQ</a><br />
        Frequently asked questions answered here.
    </div>
    <div class="clear"></div>
    <div class="grid_2">
    	<img src="<?php echo $this->baseUrl('img/tool.png'); ?>" alt="Find a solution" />
    </div>
    <div class="grid_8" style="margin-left:0;margin-right: 0;">
    	<a href="<?php echo $this->url('debt-management', 'find-a-solution'); ?>" class="debtmanhelp">Debt Solution Tool</a><br />
        Find the best debt management solution for you.
    </div>
    <div class="clear"></div>
    <br />
</div>
<div class="grid_6 push_1 blueborder justify" style="margin-bottom: 20px;">
	<h3>Customers' Stories</h3>
    <div style="padding-left: 5px;padding-right: 5px;">"An absolutely fabulous company to deal with. My case was dealt with care and consideration. </div>
        <br /><span class="italic pad_left_20"> - Mr Palmer, Liverpool</span>
</div>
<div class="grid_6 push_1 blueborder justify">
	<h3>Customers' Stories</h3>
        
    <div style="margin-left: 5px;padding-right: 5px;">"Everyone I've dealt with has been practical, helpful, calm, non-judgemental and efficient. I'd recommend this service to anyone.</div>
        <br /><span class="italic pad_left_20"> - Ms. Jones, Swansea</span>
</div>

<div class="clear"></div>
<div class="grid_14 push_3" style="margin-top: -50px;">
	<h1>How can debt management help me?</h1>
</div>
<div class="push_1 grid_16 main">
    <p>If you've taken on more monthly debt repayments than you can afford and it's difficult to pay for the essentials, <a href="<?php echo $this->url('debt-management'); ?>" title="Debt Management">debt management</a> could help make life less stressful.</p>
    <p>Perhaps your circumstances have changed or your living costs has increased. Whatever the reason, a <a href="<?php echo $this->url('debt-management', 'debt-management-plan'); ?>" title="Debt Management Plan (DMP)">debt management plan</a> could help you solve the problem.</p>
    <ul class="tick lessspace">
    	<li>Replace multiple monthly repayments with just one</li>
        <li>Reduce the monthly cost of paying your debt</li>
        <li>Could freeze or reduce interest and charges</li>
        <li>Regain control of your finances</li>
        <li>Stop demands from lenders once an arrangement is agreed</li>
    </ul>
    
    <h2>A debt management plan could:</h2>
    <ul class="tick">
    	<li>Freeze interest and charges
        	<ul>
            	<li>We will try and agree to freeze interest and charges to help you start tackling your debt sooner. Although they have no obligation to do so, lenders are often agree and recognise it's the best way forward</li>
            </ul>
        </li>
        <li style="margin-top: 30px;">Make life more affordable
        	<ul>
            	<li>A debt management plan is a new repayment plan we agree with your lenders that spreads the cost of your debt. It means you'll be paying less for longer, which will make your monthly repayments less</li>
            </ul>
        </li>
        <li style="margin-top: 30px;">No more lender demands
        	<ul>
            	<li>All the way through your debt management plan, your personal finance manager will handle all the paperwork, payments and calls. They will make sure any problems are resolved as soon as they come up and you wont have to contact any lenders throughout your time with us. No more phone calls or letters from your lenders.</li>
            </ul>
        </li>
        <li style="margin-top:70px;">Help you repay your debts without a loan
        	<ul>
            	<li>You will not need to borrow any money to reduce your repayments</li>
            </ul>
        </li>
    </ul>
    
    <h2><a href="<?php echo $this->url('debt-management', 'debt-management-plan'); ?>" title="Debt Management Plan (DMP)">Make life more affordable, now!</a></h2>
</div>
<div class="grid_6 push_1 blueborder gradientbottom pad_5" style="margin-top: 20px;">
	<strong>
    Conditions apply. Repaying your debt over a longer period of time can increase the total amount to be repaid. Your ability to obtain credit will be affected. Fees are payable when continuing service is provided. <a href="<?php echo $this->url('debt-management', 'debt-management-plan'); ?>" title="Debt Managment Plan">Read More</a>
    </strong>
</div>
