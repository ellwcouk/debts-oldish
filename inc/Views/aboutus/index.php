 <div class="grid_16 push_1">
	<h1>About Us</h1>
    <p><strong>Debts Reduced Ltd</strong>. is part of the Save Britain money Group which is a major player in the UK.</p>
<p>Everyone agrees that we would all be happier if we had a few more pounds in our pockets. Well that's exactly what Save Britain Money is trying to achieve. </p>
<p>When we started out, our company Nationwide Energy Services was insulating people’s homes and saving our customers more money on their utility bills, as well as making their homes more comfortable. NES have now established a foothold in the solar market, with installs across the country. Add Fuelswitch to the mix; and Save Britain Money has the ability to offer consumers the chance to compare and save money on energy supplied by the utility companies.</p>
<p>Over time we have added more companies to the group, offering even bigger savings for UK consumers.</p>
<p>We Claim U Gain looks after reclaiming PPI (payment protection insurance) for hundreds of consumers UK wide. The success of the business is measured in its expansion and the number of satisfied customers it has.</p>
<p>Save Me My Money is our no nonsense, online comparison site. We cover everything from car insurance, credit card deals, broadband, home insurance to TV packages and utilities, all focusing on saving consumers more money.</p>
<p>Our expansion over the last two years has been immense, seeing us taking on a new building opposite the one we already occupy at the prestigious Matrix Park. We contact 10,000 consumers per week, are in 6,000 homes per week and help over 4,000 customers per week save money on a huge range of products and services.</p>

</div>
<div class="grid_5 push_1" style="margin-top: 20px;">
<p>&nbsp;</p><img src="<?php echo $this->baseUrl('img/office.png'); ?>" alt="Our Office" />
</div>