 <div class="grid_22 push_1 main">
<h1>Questions and Answers</h1>
<p>Below is a list of questions which most clients ask when dealing with us. If your question you want answered is not in this list, do not hesitate to contact us and we will gladly answer it for you.</p>
<div class="option">
	<h6>What is a Debt Management Plan (DMP)?</h6>
    <p>The debt management plan is a tailored plan that helps a person get out of their debt problem. This plan involves working closely with the client to come to an agreement on how much they can afford to pay each month to pay off their debt.</p>

<p>We also work as a mediator with your creditors so that there is a minimum amount of stress to you. We help negotiate a sensible repayment terms to these creditors until all parties are happy with the decision.</p>

<p>However, we cannot guarantee acceptances from creditors and we should caution you that a debt management plan may increase the length of time and total amount repayable of your debts in some circumstances.</p>
</div>
<div class="option">
	<h6>What does it cost?</h6>
    <p>You will pay to us up to two monthly payments on the dates specified. This charge is for our programme set up costs which include compiling financial statements, proposals to creditors in writing and phone calls.<br />
<strong>This will be our programme set up fee and not paid to creditors.</strong><br />
We have a separate account into which we will pay the monthly payment amount. This is for the sole purpose of making payments to creditors in accordance with the programme. This account is called the Client Account. Payments are made to creditors 5 days after funds are cleared.  We shall be entitled to pay the monthly management fee from your monthly payments prior to payment being made to your creditors. We shall then make payment to creditors in accordance with your programme provided that we have sufficient funds from you.<p>

<p><strong>Each month we will charge you 17.5% of the agreed payment amount or a minimum of £35.00. All fees charged are VAT exempt.</strong></p>
</div>
<Div class="option">
	<h6>What if I keep getting letters and phone calls from my creditors?</h6>
    <p>This may happen because it takes a few days to negotiate the new terms. In the meantime they will continue with their activities culminating in a crossover period which will cause you to receive these letters.</p>

<p>When we have received your signed Letter of Authority and your first payment, you can tell your creditors to contact us. We will then be happy to deal with all letters and phone calls.</p>
</Div>
<div class="option">
	<h6>How safe is my money?</h6>
    <p>We keep a separate bank account for all the money that we receive from our clients. The amounts that you agree are then paid out of this account to your creditors. </p>
</div>
<div class="option">
	<h6>How quickly do you pay my creditors?</h6>
    <p>After receiving the money, we will normally pay your creditors within 5 working days of receipt.</p>
</div>
<div class="option">
	<h6>How will I know my creditors are being paid?</h6>
    <p>Your creditors will continue to send you your statements. As long as you keep up your payments to us, these statements will show payments from us. This will show you who is being paid and by how much.</p>

<p>A statement can be requested from us at any time.</p>
</div>
<div class="option">
	<h6>Can I pay more or less if I want to change payments?</h6>
    <p>The plan we provide is flexible. If your circumstances change, just contact us and we will discuss with you the possibility of raising or lowering your payments.</p>
</div>
<div class="option">
	<h6>Do I need to change banks?</h6>
    <p>Your bank may decide to take their money which is owed them before your creditors get their payments. This may cause inconvenience to them and decide not to take part in the plan.</p>

<p>It is advisable to open a new account with no credit, resulting in a more fair payment plan to each creditor. Giving you more control over your finances and pleasing all parties involved.</p>
</div>
<div class="option">
	<h6>Will I be able to get a mortgage or further loans/finance?</h6>
    <p>This will greatly depend on your credit status, which <strong>will be</strong> adversely affected up to 6 years by your participation in our debt management plan. </p>
</div>
<div class="option">
	<h6>Is this a loan?</h6>
    <p>Our debt management plan is not a loan. Taking out another loan will increase your monthly payments and may get you back into debt. In our professional opinion we would advise you not to take out another loan. But if your circumstances change, we would happily advise you in which direction to take your newly acquired finances.</p>
</div>
<div class="option">
	<h6>Cancellation rights and cooling off period</h6>
    <p>If you deem that the programme is not providing you benefit you are entitled to cancel at any time giving us notice. If the initial fee has been received, we will only refund this if the programme has been cancelled in writing within 14 days since receipt of this money and this period cooling off time to reconsider. If you wish to cancel your programme but the monthly.<br />
payment to your creditors has already been made we will be unable to refund any monies to you. You may cancel the programme at any time and be entitled to a refund of any monies eld by us that have not yet been sent to your creditors in the form of a payment, minus the monthly management fee, which will be retained by us for work undertaken on your behalf.<br />
You may cancel the programme at any time by giving notice in writing to us. Our authority to deal with your creditors will be rescinded. Your creditors may revert back to the original terms of your contract with them.</p>
</div>
</div>