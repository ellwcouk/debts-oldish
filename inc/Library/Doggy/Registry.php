<?php

class Doggy_Registry
{
    
    public static $_registry = array();
    
    public static function set($name, $value)
    {
        self::$_registry[$name] = $value;
        return true;
    }
    
    public static function get($name)
    {
        if(isset(self::$_registry[$name]))
        {
            return self::$_registry[$name];
        }
        
        return false;
    }
}