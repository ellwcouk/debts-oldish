<?php

class Doggy_View extends Doggy_Layout
{
 
    protected $_view;
    protected $_viewName;
    
    public function setViewScript($controller, $action)
    {
        if($this->_viewName === null)
        {
            if(isset($_SESSION['version']) && $_SESSION['version'] == 'mobile')
        {
            $action = $action . '.mobile';
        }
        if(file_exists(BASEDIR . '/inc/Views/' . $controller . '/' . $action . '.php'))
        {
            ob_start();
            include BASEDIR . '/inc/Views/' . $controller . '/' . $action . '.php';
            $this->_view = ob_get_contents();
            ob_end_clean();
        } else {
            throw new Exception('View not found. Controller: ' .$controller. ' Action: ' . $action);
        }
        } else {
            if(file_exists(BASEDIR . '/inc/Views/'.$this->_viewName.'.php'))
        {
            ob_start();
            include BASEDIR . '/inc/Views/'.$this->_viewName.'.php';
            $this->_view = ob_get_contents();
            ob_end_clean();
        } else {
            throw new Exception('View not found.');
        }
        }
        
        Doggy_Layout::getInstance()->setView($this->_view);
        
        return $this;
    }
}