<?php

class Doggy_Application
{
    protected static $_registry = array();
    
    protected $_config = array(
        'default_title' => 'Debts Reduced - Expert Debt Management',
        'default_description' => 'Consolidate all your debts into one, expert debt management plan with debts reduced. Call us today to start your unique debt management plan.',
        'email' => array(
            'sendto' => 'elliottwebsites@gmail.com',
            'config' => array(
                'ssl' => 'tls',
                'port' => '587',
                'auth' => 'login',
                'username' => 'debtsreducedltd@gmail.com',
                'password' => '1234blue'
           )
        )
    );
    
    public function __construct()
    {
        spl_autoload_register(array($this, 'autoloader'));
    }
    
    public function autoloader($fileName)
    {
        if(class_exists($fileName, false) || interface_exists($fileName, false))
        {
            return;
        }
        
        $fileName = str_replace(array('_', '\\'), '/', $fileName);
        if(file_exists(BASEDIR . '/inc/' . $fileName . '.php'))
        {
            include BASEDIR . '/inc/' .$fileName . '.php';
            return;
        } elseif(file_exists(BASEDIR . '/inc/Library/' . $fileName . '.php'))
        {
           include BASEDIR . '/inc/Library/' . $fileName . '.php';
           return;
        }
    }
    
    public function run()
    {
        session_start();
        
        // do config
        foreach($this->_config as $key => $value)
        {
            Doggy_Registry::set($key, $value);
        }
        
        
        $uri = $this->_getUrl();
        Doggy_Registry::set('uri', $uri);
        if($this->isDispatchable($uri['controller'], $uri['action']))
        {
            Doggy_Registry::set('controller', ucfirst($uri['controller']));
            Doggy_Registry::set('action', $uri['action']);
            $controller = $uri['controller'];
            $action = $uri['action'];
        } else {
            if($this->viewExists($uri['controller'], $uri['action']))
            {
                Doggy_Registry::set('controller', '');
                Doggy_Registry::set('action', '');
                $controller = $uri['controller'];
                $action = $uri['action'];
            } else {
                Doggy_Registry::set('controller', 'error');
                Doggy_Registry::set('action', 'error');
                $controller = 'error';
                $action = 'error';
            }
        }
        
        // do the controller
        $className = 'Controllers_' . ucfirst($controller);
        $controllerClass = new $className;
        if(method_exists($controllerClass, 'init'))
        {
            $controllerClass->init();
        }
        // set some vars here...
       
        $actionName = $action . 'Action';
        $controllerClass->$actionName();
        
        $layout = Doggy_Layout::getInstance();
        $view = new Doggy_View($this);
        $view->setViewScript($controller, $action);
        echo $layout->getLayout();
    }
    
    public function isDispatchable($controller, $action)
    {
        $className = 'Controllers_' . ucfirst($controller);
        if(!class_exists($className, true))
        {
            return false;
        }
        
        $class = new $className;
        $action = $action . 'Action';
        
        if(!method_exists($class, $action))
        {
            unset($class);
            return false;
        }
        
        unset($class);
        return true;
    }
    
    public function viewExists($controller, $action)
    {
        if(file_exists(BASEDIR . 'inc/Views/' . $controller . '/' . $action . '.php'))
        {
            return true;
        }
        
        return false;
    }
    
    
    protected function _getUrl()
    {
        $url = $this->_cleanUrl($_SERVER['REQUEST_URI']);
        
        $parts = explode('/', $url);
        
        $uri = array();
        
        if(!isset($parts[0]) || empty($parts[0]))
        {
            $uri['controller'] = 'index';
        } else {
            $uri['controller'] = $parts[0];
        }
        
        if(!isset($parts[1]) || empty($parts[1]))
        {
            $uri['action'] = 'index';
        } else {
            $uri['action'] = $parts[1];
        }
        unset($parts[0], $parts[1]);
        
        $keyHolder = '';
        $i = 0;
        foreach($parts as $value)
        {
            if($i == 0)
            {
                $keyHolder = $value;
                $i++;
            } else {
                $uri[$keyHolder] = $value;
                $i--;
            }
        }
        
        return $uri;
    }
    
    protected function _cleanUrl($url)
    {
        // Remove the base url
        $url = str_replace(Doggy_Helpers::baseUrl(), '/', $url);
        $url = strtolower(preg_replace('/[^a-zA-Z0-9\/]/', '', $url));
        if(substr($url, 0, 1) == '/')
        {
            $url = substr($url, 1);
        }  
        
        return $url;
    }
    
   
    
}