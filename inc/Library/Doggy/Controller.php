<?php

class Doggy_Controller
{
    
    public function detectAjax()
    {
        return Doggy_Helpers::detectAjax();
    }
    
    public function detectMobile()
    {
        return Doggy_Helpers::detectMobile();
    }
    
    public function prependScript($script)
    {
        Doggy_Layout::getInstance()->prependScripts .= $script;
    }
    
    public function url($controller, $action = null, $params = array())
    {
        return Doggy_Helpers::generateUrl($controller, $action, $params);
    }
    
    public function isPost()
    {
        if (strtoupper($_SERVER['REQUEST_METHOD']) == 'POST')
        {
            return true;
        }
        
        return false;
    }
    
    public function layout() {
        return Doggy_Layout::getInstance();
    }
}