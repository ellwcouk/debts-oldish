<?php

class Doggy_Layout
{
    
    protected $_layoutName = 'default';
    protected $_content;
    protected $_layout;
    public $appendScripts;
    public $prependScripts;
    public $metaTags;
    public $description;
    public $title;
    public static $instance;
    
    public static function getInstance()
    {
        if(!self::$instance)
        {
            self::$instance = new Doggy_Layout;
        }
        
        return self::$instance;
    }
    
    public function ___set($name, $value)
    {
        $this->{$name} = $value;
    }
    
    public function ___get($value)
    {
        if(!isset($this->{$value}))
        {
            $this->{$value} = '';
        }
    }
    
    public function setTitle($title)
    {
        $this->_title = $title;
        return self::getInstance();
    }
    
    public function getTitle()
    {
        return $this->_title;
    }
    
    public function setLayoutName($layoutName)
    {
        $this->_layoutName = $layoutName;
        return self::getInstance();
    }
    
    public function getLayoutName()
    {
        return $this->_layoutName;
    }
    
    
    public function content()
    {
        return $this->_content;
    }
    
    public function baseUrl($url = null)
    {
        return Doggy_Helpers::baseUrl($url);
    }
    
    public function setView($view)
    {
        $this->_content = $view;
    }
    
    public function getLayout()
    {
        if(isset($_SESSION['version']) && $_SESSION['version'] == 'mobile')
        {
            $this->setLayoutName($this->getLayoutName() . '.mobile');
        }
        $this->title = $this->title == '' ? Doggy_Registry::get('default_title') : $this->title;
        $this->description = $this->description == '' ? Doggy_Registry::get('default_description') : $this->description;
        if(file_exists(BASEDIR . '/inc/Layouts/' . $this->getLayoutName() . '.php'))
        {
            ob_start();
            include BASEDIR . '/inc/Layouts/' . $this->getLayoutName() . '.php';
            $this->_layout = ob_get_contents();
            ob_end_clean();
        } else {
            ob_start();
            echo $this->content();
            $this->_layout = ob_get_contents();
            ob_end_clean();
        }
        
        return $this->_layout;
      
    }
    
    /**
     * The helpers... 
     */
    public function url($controller, $action = null, $params = array())
    {
        return Doggy_Helpers::generateUrl($controller, $action, $params);
    }
}