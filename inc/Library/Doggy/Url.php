<?php
/**
 * Jariah Framework
 * 
 * @category Jariah
 * @package Jariah\Url
 * @copyright Copyright (c) 2012 James Elliott
 * @license  http://creativecommons.org/licenses/by-sa/3.0/
 * @version 0.2
 */
class Url
{
    
    /**
     * Returns the whole URL string (http(s)://example.com/whatever
     * @return string
     */
    public function getFullUrl()
    {
        $protocol = $this->getProtocol();
        $port = $this->getPort() == '80' ? '' : ':' . $this->getPort();
        
        return $protocol . '://' . $_SERVER['SERVER_NAME'] . $port . $_SERVER['REQUEST_URI'];
    }
    
    /**
     * Gets the protocol. Can redirect to http or https with a HTTP code
     * @param string $redirect
     * @param string $code
     * @return string 
     */
    public function getProtocol($redirect = 'http', $code = 'HTTP/1.0 101 Switching Protocols')
    {
        if(!empty($_SERVER['HTTPS']))
        {
            $protocol = 'https';
        } else {
            $protocol = 'http';
        }
        
        if($redirect != $protocol)
        {
            $url = $this->getFullUrl();
            $url = str_replace($redirect, $protocol, $url);
            header($code);
            header('Location: ' . $url);
        }
        
        return $protocol;
    }
    
    /**
     * Cleans the with permitted URL characters from configs/system.php.
     * @param string $url
     * @return string 
     */
    public function cleanUrl($url = null)
    {
        if($url === null)
        {
            $url = $_SERVER['REQUEST_URI'];
        }
        
        // Remove the base url
        $url = str_replace($this->_getBaseUrl(), '', $url);
        $url = strtolower(preg_replace(Registry::get('permitted_url_chars'), '', $url));
        
        return $url;
    }
    
    /**
     * Get the URL vars.
     * @return array
     */
    public function getUri($cleanUrl = true)
    {
        if(Registry::get('use_get_vars') == '1')
        {
            return $this->_fromGet($cleanUrl);
        }
        
        return $this->_fromRewrite($cleanUrl);
    }
    
    /**
     * Gets the port
     * @return string
     */
    public function getPort()
    {
        return $_SERVER['SERVER_PORT'];
    }
    
    /**
     * Gets the URI in an array format from apache2's mod_rewrite
     * @return array
     */
    protected function _fromRewrite($cleanUrl = true)
    {
        if($cleanUrl == true)
        {
            $url = $this->cleanUrl($_SERVER['REQUEST_URI']);
        } else {
            $url = str_replace($this->_getBaseUrl(), '', $_SERVER['REQUEST_URI']);
        }
        
        $parts = explode('/', $url);
        
        $uri = array();
        
        if(!isset($parts[0]) || empty($parts[0]))
        {
            $uri['controller'] = 'index';
        } else {
            $uri['controller'] = $parts[0];
        }
        
        if(!isset($parts[1]) || empty($parts[1]))
        {
            $uri['action'] = 'index';
        } else {
            $uri['action'] = $parts[1];
        }
        unset($parts[0]);
        unset($parts[1]);
        
        $keyHolder = '';
        $i = 0;
        foreach($parts as $value)
        {
            if($i == 0)
            {
                $keyHolder = $value;
                $i++;
            } else {
                $uri[$keyHolder] = $value;
                $i--;
            }
        }
        
        return $uri;
    }
    
    /**
     * Gets the URI as an array from GET variables
     * @return array
     */
    protected function _fromGet($cleanurl = true)
    {
        $params = array();
        $params['controller'] = isset($_GET['c']) ? $this->cleanUrl($_GET['c']) :
            'index';
        $params['action'] = isset($_GET['a']) ? $this->cleanUrl($_GET['a']) :
            'index';
        
        foreach($_GET as $key => $value)
        {
            if($key == 'a' || $key == 'c')
            {
                continue;
            }
            
            $params[$key] = $value;
        }
        
        return $params;
    }
    
    /**
     * Returns the baseUrl
     * @return string
     */
    protected function _getBaseUrl()
    {
        return substr($_SERVER['PHP_SELF'], 0, -9);
    }
}