<?php

class Controllers_Gethelpnow extends Doggy_Controller
{
    
    public function init()
    {
        if($this->isPost())
        {
            // call back form
            if(isset($_POST['callback']))
            {
                $name = $_POST['callbackname'];
                $number = $_POST['callbacknumber'];
                
                $emailconfig = Doggy_Registry::get('email');
                
                $transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $emailconfig['config']);
                $mail = new Zend_Mail();
                $mail->setBodyHtml('Callback from: ' . $name . ' <br />Number: ' . $number);
                $mail->setFrom('debtsreducedltd@gmail.com', 'Debts Reduced Website');
                $mail->addTo($emailconfig['sendto'], 'Debts Reduced');
                $mail->setSubject('Debts Reduced Callback');
                $mail->send($transport);
                
                echo 'Thank you. We will contact you as soon as we can.';
            }
            // Do the lookup for the postcode...
            if(isset($_POST['lookup']))
            {
                $url = 'http://maps.googleapis.com/maps/api/geocode/xml?address='.urlencode($_POST['postcode']).'&sensor=false';
		$parsedXML = simplexml_load_file($url);
		if($parsedXML->status != "OK") {
			die("There has been a problem: " . $parsedXML->status);
		}
		$latlong = array();
		foreach($parsedXML->result->geometry as $geo)
		{
			$latlong = array('lat' => (string)$geo->location->lat, 'lng' => (string)$geo->location->lng);
		}
		$url = 'http://maps.googleapis.com/maps/api/geocode/xml?latlng='.$latlong['lat'].','.$latlong['lng'].'&sensor=false';
		$parsedXML = simplexml_load_file($url);
		if($parsedXML->status != "OK") {
			die("There has been a problem: " . $parsedXML->status);
		}
		$myAddress = array();
		foreach($parsedXML->result->address_component as $component)
		{
			if(is_array($component->type)) {
				$type = (string)$component->type[0];
			} else {
				$type = (string)$component->type;
			}

			$myAddress[$type] = (string)$component->long_name;
		}

		if(isset($myAddress['administrative_area_level_3']))
		{
			$county = $myAddress['administrative_area_level_3'];
		} else if(isset($myAddress['administrative_area_level_2']))
		{
			$county = $myAddress['administrative_area_level_2'];
		} else if(isset($myAddress['administrative_area_level_1']))
		{
			$county = $myAddress['administrative_area_level_1'];	
		} else {
			$county = 'unknown';
		}
		echo $myAddress['route'] . ',', $myAddress['locality'] . ',' . $county;
            }
            
            if (isset($_POST['bigform']))
            {
                $emailconfig = Doggy_Registry::get('email');

                $transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $emailconfig['config']);
                $mail = new Zend_Mail();
                ob_start();
                include(BASEDIR . '/inc/scripts/gethelpnow.php');
                $email = ob_get_contents();
                ob_end_clean();
                $mail->setBodyHtml($email);
                $mail->setFrom('debtsreducedltd@gmail.com', 'Debts Reduced Website');
                $mail->addTo($emailconfig['sendto'], 'Debts Reduced');
                $mail->setSubject('Debts Reduced Slider');
                $mail->send($transport);
                echo 'Thank you. We will contact you as soon as we can.';
                die();
            }
            
            die();
        }
    }
    
    public function indexAction()
    {
        $this->prependScript('
<script type="text/javascript">
function address(data) {
	data = data.split(",");
	$("#addressline1").val(data[0])
	$("#town").val(data[1]);
	$("#county").val(data[2]);
        $("#overlay").hide();
}
$(function() {
    $("#form1").on("submit", function(ev) {
        ev.preventDefault();
            ev.stopPropagation();

            if(!$(\'#agree\').is(\':checked\')) {
                alert("Please accept the privacy policy");
                return;
            }
            var form = $(\'#callbackfrm22\').serialize() + \'&bigform=true\';
            $(\'#overlaybox\').html(\'<img src="\' + window.baseurl + \'img/loader.gif" />\');
            function afterfunction(data) {
                alert(data);
                $(\'#overlay\').hide();
            }
            debts.utils.ajax(window.baseurl + \'get-help-now\', form, afterfunction);
    });
	$("#lookup").on("click", function() {
		if($("#postcode").val() == "")
		{
			alert("Please enter a postcode");
		} else {
			debts.utils.ajax("'.Doggy_Layout::getInstance()->baseUrl('get-help-now').'", $("#callbackfrm2").serialize() + "&lookup=yes&name=yes", address, "html");
		}
	});
});
</script>');
    }
}