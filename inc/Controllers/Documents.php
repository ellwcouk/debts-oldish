<?php
class Controllers_Documents extends Doggy_Controller
{
    
    public function init()
    {
        
    }
    
    public function indexAction()
    {
        
    }
    
    public function downloadAction()
    {
        $uri = Doggy_Registry::get('uri');
        if(file_exists(BASEDIR . '/downloads/'.$uri['file'].$uri['version'].'.pdf'))
        {
            header('Content-disposition: attachment; filename='.$uri['file'].$uri['version'].'.pdf');
            header('Content-type: application/pdf');
            readfile(BASEDIR . '/downloads/'. $uri['file'].$uri['version'] . '.pdf');
            die();
        }
    }
    
}