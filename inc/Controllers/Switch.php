<?php

class Controllers_Switch extends Doggy_Controller
{
    
    public function init()
    {
        
    }
    
    public function mobileAction()
    {
        $_SESSION['version'] = 'mobile';
        header('Location: ' . Doggy_Helpers::baseUrl());
        echo 'If you are not redirected, click <a href="'.Doggy_Helpers::baseUrl().'">Here</a>';
        die();
    }
    
    public function desktopAction()
    {
        $_SESSION['version'] = 'default';
        header('Location: ' . Doggy_Helpers::baseUrl());
        echo 'If you are not redirected, click <a href="'.Doggy_Helpers::baseUrl().'">Here</a>';
        die();
    }
}