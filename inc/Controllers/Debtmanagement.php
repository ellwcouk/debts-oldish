<?php

class Controllers_Debtmanagement extends Doggy_Controller
{
    
    public function init()
    {
        if(isset($_POST['slider']))
        {
            $this->sliderEmail();
            echo 'Thank you. We will contact you as soon as we can.';
            die();
        }
    }
    
    public function sliderEmail()
    {
        $emailconfig = Doggy_Registry::get('email');
                
        $transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $emailconfig['config']);
        $mail = new Zend_Mail();
        ob_start();
        include(BASEDIR . '/inc/scripts/sliderEmail.php');
        $email = ob_get_contents();
        ob_end_clean();
        $mail->setBodyHtml($email);
        $mail->setFrom('debtsreducedltd@gmail.com', 'Debts Reduced Website');
        $mail->addTo($emailconfig['sendto'], 'Debts Reduced');
        $mail->setSubject('Debts Reduced Slider');
        $mail->send($transport);
    }
    
    public function indexAction()
    {
        
    }
    
    public function howadebtmanagementplanworksAction()
    {
        
    }
    
    public function debtmanagementplanAction()
    {
        
    }
    
    public function findasolutionAction()
    {
        
        $v = 5;
        $uri = Doggy_Registry::get('uri');
        if(isset($uri['v']))
        {
            $v = $uri['v'];
        }
        $this->layout()->v = $v;
        $this->layout()->prependScripts = '<script type="text/javascript"> 
         $(function() {
	             $( ".slider" ).slider({
			    animate: true,
                range: "min",
                value: '.$v.',
                min: 1,
                max: 100,
				step: 1,
                
				//this gets a live reading of the value and prints it on the page
                slide: function( event, ui ) {
					if(ui.value == "100") {
						$( ".sliderresult").append("+");
					} else {
							$(".sliderresult").html($(".sliderresult").html().replace("+", ""));
					}
					$( "#slider-result" ).html( ui.value );
					$("#sliderresulta").attr("href", "'.Doggy_Layout::getInstance()->baseUrl('debt-management/find-a-solution').'/v/" + ui.value);
                },

				//this updates the hidden form field so we can submit the data using a form
                change: function(event, ui) { 
                $(\'#hidden\').attr(\'value\', ui.value);
                }
			
				});
			$("#sliderresulta").on("click", function(ev) {
				ev.preventDefault();
				ev.stopPropagation();
			});

		 });
        </script>
		<script type="text/javascript" src="'.Doggy_Layout::getInstance()->baseUrl('js/slider.js') .'"></script>';
    }
}