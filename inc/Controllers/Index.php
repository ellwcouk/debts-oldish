<?php

class Controllers_Index extends Doggy_Controller
{
    
    public function indexAction()
    {
        $this->prependScript('    <script type="text/javascript"> 
         $(function() {
	             $( ".slider" ).slider({
			    animate: true,
                range: "min",
                value: 5,
                min: 1,
                max: 100,
				step: 1,
                slide: function( event, ui ) {
					if(ui.value == "100") {
						$( ".sliderresult").append("+");
					} else {
							$(".sliderresult").html($(".sliderresult").html().replace("+", ""));
					}
                    $( "#slider-result" ).html( ui.value );
					$("#sliderresulta").attr("href", "'.$this->url('debt-management', 'find-a-solution', array('v' => '')).'" + ui.value);
                },

				//this updates the hidden form field so we can submit the data using a form
                change: function(event, ui) { 
                $("#hidden").attr("value", ui.value);
                }
			
				});

		 });
        </script>');
    }
}