<?php
class Controllers_Faq extends Doggy_Controller
{
    
    public function init()
    {
        
    }
    
    public function indexAction()
    {
        $this->prependScript("<script type=\"text/javascript\">
$(function() {
	$('.option p').addClass('hidden');
	$('.option h6').css('cursor', 'pointer');
	$('.option').on('click', function() {
		if($(this).hasClass('open')) {
			$(this).find('p').hide();
			$(this).removeClass('open');
		} else {
			$(this).find('p').show();
			$(this).addClass('open');
		}
	});
});
</script>");
    }
}