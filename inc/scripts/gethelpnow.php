<table width="100%">
    <tr>
        <td>Title:</td>
        <td><?php echo $_POST['title']; ?></td>
    </tr>
    <tr>
        <td>Forename</td>
        <td><?php echo $_POST['firstname']; ?></td>
    </tr>
    <tr>
        <td>Surname</td>
        <td><?php echo $_POST['surname']; ?></td>
    </tr>
    <tr>
        <td>Address Line 1</td>
        <td><?php echo $_POST['housenumber'] . ' ' . $_POST['addressline1']; ?></td>
    </tr>
    <tr>
        <td>Address Line 2</td>
        <td><?php echo $_POST['addressline2']; ?></td>
    </tr>
    <tr>
        <td>Town</td>
        <td><?php echo $_POST['town']; ?></td>
    </tr>
    <tr>
        <td>county</td>
        <td><?php echo $_POST['county']; ?></td>
    </tr>
    <tr>
        <td>Postcode</td>
        <td><?php echo $_POST['postcode']; ?></td>
    </tr>
    <tr>
        <td>Phone</td>
        <td><?php echo $_POST['phone']; ?></td>
    </tr>
    <tr>
        <td>email</td>
        <td><?php echo $_POST['email']; ?></td>
    </tr>
    <tr>
        <td>Approx. Debt</td>
        <td><?php echo $_POST['approxdebt']; ?></td>
    </tr>
    <tr>
        <td>How many people do you owe</td>
        <td><?php echo $_POST['peopleowe']; ?></td>
    </tr>
    <tr>
        <td>Current monthly debt repayment</td>
        <td><?php echo $_POST['monthlydebt']; ?></td>
    </tr>
    <tr>
        <td>Realistically afford</td>
        <td><?php echo $_POST['afford']; ?></td>
    </tr>
    <tr>
        <td>Employment status</td>
        <td><?php echo $_POST['employment']; ?></td>
    </tr>
    <tr>
        <td>How did you hear about us</td>
        <td><?php echo $_POST['hearabout']; ?></td>
    </tr>
</table>