<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
        <?php echo $this->metaTags; ?>
        <meta name="description" content="<?php echo $this->description; ?>" />
        <title><?php echo $this->title; ?></title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->baseUrl('favicon.ico'); ?>" />
        <link rel="icon" type="image/vnd.microsoft.icon" href="<?php echo $this->baseUrl('favicon.ico'); ?>" />
        <link rel="icon" type="image/png" href="<?php echo $this->baseUrl('favicon.png'); ?>" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,700,800,300' rel='stylesheet' type='text/css' />
        <link href="<?php echo $this->baseUrl('css/reset.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo $this->baseUrl('css/960_24_col.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo $this->baseUrl('css/style.css'); ?>" rel="stylesheet" type="text/css" />
        <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-30406343-1']);
  _gaq.push(['_setDomainName', 'debts-reduced.co.uk']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

        <?php echo $this->appendScripts; ?>
<!--[if IE]>
<style type="text/css">
.phone, .bluebox, .blueborder { behavior: url(<?php echo $this->baseUrl('css/PIE.htc'); ?>); }
h1 {font-size: 23px;}
</style>
<![endif]-->
        <!--[if lte IE 7]>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/chrome-frame/1/CFInstall.min.js"></script>
<style type="text/css">
.chromeFrameInstallDefaultStyle {
	width: 100%; /* default is 800px */
	border: 5px solid blue;
	z-index: 9999999999;
}
</style>
<script type="text/javascript">
window.attachEvent("onload", function() {
	CFInstall.check({
		mode: "overlay", // the default
		node: "prompt"
	});
});
</script>
<![endif]-->
    </head>
    <body>
        <div id="overlay">
            <div id="overlaybox"></div>
        </div>
        <div id="ajax_panel"></div>
        <div class="container_24">
            <div class="topcontent grid_7 push_13">
                <ul class="topnav">
                    <li><a href="<?php echo $this->url('about-us'); ?>" title="About Us">About Us</a></li>
                    <li><a href="<?php echo $this->url('get-help-now'); ?>" title="Contact Us">Contact Us</a></li>
                    <li class="last"><a href="<?php echo $this->url('login'); ?>" title="login">Login</a></li>
                </ul>
            </div>
            <div class="clear"></div>
            <div class="grid_8">
                <a href="<?php echo $this->url('index'); ?>" title="Home"><img src="<?php echo $this->baseUrl('img/logo.png'); ?>" alt="Debts Reduced - Expert Debt Management" /></a>
            </div>
            <div class="grid_6 phone push_1">
                <h6>Call us now on:</h6>
                <h1>0844 335 1355</h1>
            </div>
            <div class="grid_6 phone push_3" id="requestcallback">
                <h6>Request a call back</h6>
                <p>Click here to request a callback and we will call at a time to suit you</p>
            </div>
            <div class="clear"></div>
            <div class="content">
                <div class="grid_24">
                    <ul class="nav">
                    <?php
                    $nav = array(
                        array('Home', 'Home Page', array('c' => 'index', 'a' => 'index')),
                        array('Debt Management', 'Debt Management', array('c' => 'debt-management', 'a' => 'index'), array(
                            array('How it works', 'How a debt management plan works', array('c' => 'debt-management', 'a' => 'how-a-debt-management-plan-works')),
                            array('Debt Management Plan', 'Debt Management Plan', array('c' => 'debt-management', 'a' => 'debt-management-plan')),
                            array('Find a Solution', 'Find a solution with our solution finder tool', array('c' => 'debt-management', 'a' => 'find-a-solution'))
                        )),
                        array('Other Solutions', 'What other solutions are there for me?', array('c' => 'other-solutions', 'a' => 'index'), array(
                            array('Scottish Trust Deed', 'Scottish Trust Deed', array('c' => 'other-solutions', 'a' => 'scottish-trust-deed')),
                            array('Bankruptcy', 'Bankruptcy', array('c' => 'other-solutions', 'a' => 'bankruptcy')),
                            array('Individual Voluntary Arrangement (IVA)', 'Individual Voluntary Arrangement0', array('c' => 'other-solutions', 'a' => 'individual-voluntary-arrangement')),
                            array('Re-Mortgaging', 'Re-Mortgaging', array('c' => 'other-solutions', 'a' => 'remortgaging')),
                            array('Find a Solution', 'Find a solution with out solutions finder tool', array('c' => 'debt-management', 'a' => 'find-a-solution'))
                        )),
                        array('Get Help Now', 'Contact Us for help', array('c' => 'get-help-now', 'a' => 'index')),
                        array('FAQ', 'FAQ - Frequently Asked Questions', array('c' => 'faq', 'a' => 'index')),
                        array('About Us', 'About Us', array('c' => 'about-us', 'a' => 'index')),
                        array('Documents', 'Documents', array('c' => 'documents', 'a' => 'index'))
                    );
                    $navi = '';
                    foreach($nav as $navigation)
                    {
                        $submenu = '';
                        if(isset($navigation[3]))
                        {
                            $submenu = '<ul class="subnav">';
                            foreach($navigation[3] as $sub)
                            {
                                if($sub[2]['a'] == 'index')
                                {
                                    $link = $this->url($sub[2][c]);
                                } else {
                                    $link = $this->url($sub[2]['c'], $sub[2]['a']);
                                }
                                $submenu .= '<li><a href="'.$link.'" title="'.$sub[1].'">' . $sub[0] . '</a></li>';
                            }
                            
                            $submenu .= '</ul>';
                        }
                        if($navigation[2]['a'] == 'index')
                        {
                            $link = $this->url($navigation[2]['c']);
                        } else {
                            $link = $this->url($navigation['2']['c'], $navigation['2']['a']);
                        }
                        $navi .= '<li><a href="'.$link.'" title="'.$navigation[1].'">' . $navigation[0] . '</a> '. $submenu .'</li>' . PHP_EOL;
                    }
                    
                    echo $navi;
                    ?>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>
            <?php echo $this->content(); ?>
            <div class="clear"></div>
            <div class="grid_24">
                <hr />
            </div>
            <div class="footer grid_13">
                <a href="<?php echo $this->url('sitemap'); ?>" title="Sitemap">Sitemap</a>
                <a href="<?php echo $this->url('terms', 'privacy-policy'); ?>" title="Pivacy Policy">Privacy Policy</a>
                <a href="<?php echo $this->url('documents'); ?>" title="Documents">Documents</a>
                <a href="<?php echo $this->url('terms', 'complaints'); ?>" title="Complaints">Complaints</a>
                <a href="<?php echo $this->url('switch', 'mobile'); ?>" title="Switch To Mobile Site">Switch to Mobile Site</a>
            </div>
            <div class="footer grid_11" style="text-align:right;font-size:11px;">
        	&copy; 2012 Debts Reduced Ltd.<br />
                Company Number: 07385006 | CCA licence no.: 643996 | Data Protection: Z2896846<br />Save Britain Money Data Protection: Z6715799
            </div>
            <div class="footer grid_24" style="font-size: 11px;">
                <br />
                <p><strong>Debt Management Plans Explanation of costs/fees and affect on your credit rating:</strong><br />
                You will pay to us the first two monthly payments on the dates specified, this will be our programme set up fee and not paid to creditors.This charge is for our programme set up costs which include compiling financial statements, proposals to creditors in writing and phone calls. This may affect your credit rating and will put you further in arrears. Each month thereafter we will charge you 17.5% of the agreed payment amount or a minimum of £35.00. All fees charged are VAT exempt. Please note that any retained payments may place you further into arrears at the beginning of your programme.</p>

                <p><strong>Insolvency Service: <a href="http://www.insolvency.gov.uk/pdfs/guidanceleafletspdf/indebt-web.pdf" target="_blank">In Debt? Dealing with your creditors?</a></strong></p>

                <p><strong>Cooling off:</strong><br />
                If you deem that the programme is not providing you benefit you are entitled to cancel at any time giving us notice. If the initial fee has been received, we will only refund this if the programme has been cancelled in writing within 14 days of signing the agreement and this period cooling off time to reconsider. If you wish to cancel your programme but the monthly payment to your creditors has already been made we will be unable to refund any monies to you. You may cancel the programme at any time and be entitled to a refund of any monies held by us that have not yet been sent to your creditors in the form of a payment, minus the monthly management fee, which will be retained by us for work undertaken on your behalf.<br />
                You may cancel the programme at any time by giving notice in writing to us. Our authority to deal with your creditors will be rescinded. Your creditors may revert back to the original terms of your contract with them.</p>
            </div>
        </div>
        <script type="text/javascript">window.baseurl = '<?php echo $this->baseUrl(''); ?>';</script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.js"></script>
        <script type="text/javascript" src="<?php echo $this->baseUrl('js/debts.js'); ?>"></script>
        <?php echo $this->prependScripts; ?>
    </body>
</html>
