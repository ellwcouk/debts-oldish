<!DOCTYPE html>
<html>
    <head>
        <title>Debts Reduced - Expert Debt Management</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <style type="text/css">
        /* `XHTML, HTML4, HTML5 Reset
----------------------------------------------------------------------------------------------------*/

a,
abbr,
acronym,
address,
applet,
article,
aside,
audio,
b,
big,
blockquote,
body,
canvas,
caption,
center,
cite,
code,
dd,
del,
details,
dfn,
dialog,
div,
dl,
dt,
em,
embed,
fieldset,
figcaption,
figure,
font,
footer,
form,
h1,
h2,
h3,
h4,
h5,
h6,
header,
hgroup,
hr,
html,
i,
iframe,
img,
ins,
kbd,
label,
legend,
li,
mark,
menu,
meter,
nav,
object,
ol,
output,
p,
pre,
progress,
q,
rp,
rt,
ruby,
s,
samp,
section,
small,
span,
strike,
strong,
sub,
summary,
sup,
table,
tbody,
td,
tfoot,
th,
thead,
time,
tr,
tt,
u,
ul,
var,
video,
xmp {
  border: 0;
  margin: 0;
  padding: 0;
  font-size: 100%;
}

html,
body {
  height: 100%;
}

article,
aside,
details,
figcaption,
figure,
footer,
header,
hgroup,
menu,
nav,
section {
/*
  Override the default (display: inline) for
  browsers that do not recognize HTML5 tags.

  IE8 (and lower) requires a shiv:
  http://ejohn.org/blog/html5-shiv
*/
  display: block;
}

b,
strong {
/*
  Makes browsers agree.
  IE + Opera = font-weight: bold.
  Gecko + WebKit = font-weight: bolder.
*/
  font-weight: bold;
}

img {
  color: transparent;
  font-size: 0;
  vertical-align: middle;
/*
  For IE.
  http://css-tricks.com/ie-fix-bicubic-scaling-for-images
*/
  -ms-interpolation-mode: bicubic;
}

li {
/*
  For IE6 + IE7.
*/
  display: list-item;
}

table {
  border-collapse: collapse;
  border-spacing: 0;
}

th,
td,
caption {
  font-weight: normal;
  vertical-align: top;
  text-align: left;
}

q {
  quotes: none;
}

q:before,
q:after {
  content: '';
  content: none;
}

sub,
sup,
small {
  font-size: 75%;
}

sub,
sup {
  line-height: 0;
  position: relative;
  vertical-align: baseline;
}

sub {
  bottom: -0.25em;
}

sup {
  top: -0.5em;
}

svg {
/*
  For IE9.
*/
  overflow: hidden;
}
/*
  960 Grid System ~ Text CSS.
  Learn more ~ http://960.gs/

  Licensed under GPL and MIT.
*/

/* `Basic HTML
----------------------------------------------------------------------------------------------------*/

body {
  font: 13px/1.5 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
}

hr {
  border: 0 #ccc solid;
  border-top-width: 1px;
  clear: both;
  height: 0;
}

/* `Headings
----------------------------------------------------------------------------------------------------*/

h1 {
  font-size: 25px;
}

h2 {
  font-size: 23px;
}

h3 {
  font-size: 21px;
}

h4 {
  font-size: 19px;
}

h5 {
  font-size: 17px;
}

h6 {
  font-size: 15px;
}

/* `Spacing
----------------------------------------------------------------------------------------------------*/

ol {
  list-style: decimal;
}

ul {
  list-style: disc;
}

li {
  margin-left: 30px;
}

p,
dl,
hr,
h1,
h2,
h3,
h4,
h5,
h6,
ol,
ul,
pre,
table,
address,
fieldset,
figure {
  margin-bottom: 20px;
}
.container_6,.container_8{margin-left:auto;margin-right:auto;width:480px}.grid_1,.grid_2,.grid_3,.grid_4,.grid_5,.grid_6,.grid_7,.grid_8{display:inline;float:left;margin-left:10px;margin-right:10px}.container_6 .grid_3,.container_8 .grid_4{width:220px}.container_6 .grid_6,.container_8 .grid_8{width:460px}.alpha{margin-left:0}.omega{margin-right:0}.container_6 .grid_1{width:60px}.container_6 .grid_2{width:140px}.container_6 .grid_4{width:300px}.container_6 .grid_5{width:380px}.container_8 .grid_1{width:40px}.container_8 .grid_2{width:100px}.container_8 .grid_3{width:160px}.container_8 .grid_5{width:280px}.container_8 .grid_6{width:340px}.container_8 .grid_7{width:400px}.container_6 .prefix_3,.container_8 .prefix_4{padding-left:240px}.container_6 .prefix_6,.container_8 .prefix_8{padding-left:480px}.container_6 .prefix_1{padding-left:80px}.container_6 .prefix_2{padding-left:160px}.container_6 .prefix_4{padding-left:320px}.container_6 .prefix_5{padding-left:400px}.container_8 .prefix_1{padding-left:60px}.container_8 .prefix_2{padding-left:120px}.container_8 .prefix_3{padding-left:180px}.container_8 .prefix_5{padding-left:300px}.container_8 .prefix_6{padding-left:360px}.container_8 .prefix_7{padding-left:420px}.container_6 .suffix_3,.container_8 .suffix_4{padding-right:240px}.container_6 .suffix_1{padding-right:80px}.container_6 .suffix_2{padding-right:160px}.container_6 .suffix_4{padding-right:320px}.container_6 .suffix_5{padding-right:400px}.container_8 .suffix_1{padding-right:60px}.container_8 .suffix_2{padding-right:120px}.container_8 .suffix_3{padding-right:180px}.container_8 .suffix_5{padding-right:300px}.container_8 .suffix_6{padding-right:360px}.container_8 .suffix_7{padding-right:420px}.clear{clear:both;display:block;overflow:hidden;visibility:hidden;width:0;height:0}.clearfix:after{clear:both;content:' ';display:block;font-size:0;line-height:0;visibility:hidden;width:0;height:0}.clearfix{display:inline-block}* html .clearfix{height:1%}.clearfix{display:block}
 
/* styles */
body {
    font-family: Arial, sans-serif;
    color: #444;
}
#nav {
    font-size: 18px;
    margin-top: 30px;
    margin-left: 20px;
}
#nav a {
    color: #09F;
    text-decoration: none;
}
.bluebox {
	background-color: #006699;
	border: 1px solid #999;	
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
	color: #FFF;
	padding: 10px;
}
h1 {
    margin-bottom: 0;
}
.margin10 {
    margin: 10px;
}
.footer {
    font-size: 10px;
}
.center {
    text-align: center;
}

    </style>
    </head>
    <body>
        <div class="container_6">
        <div class="grid_2">
            <a href="<?php echo $this->url('index'); ?>" title="Home"><img src="<?php echo $this->baseUrl('img/logo.mobile.png'); ?>" alt="Debts Reduced Logo" /></a>
        </div>
            <div class="grid_4">
                <div id="nav"><a href="<?php echo $this->url('index'); ?>">Home</a> | <a href="<?php echo $this->url('get-help-now'); ?>">Get Help Now</a> | <a href="<?php echo $this->url('switch', 'desktop'); ?>">Full Site</a></div>
            </div>
        <div class="clear"></div>
        <?php echo $this->content(); ?>
        <div class="clear"></div>
        <div class="footer grid_6 center">
            &copy; 2012 Debts Reduced Ltd. Company Number: 07385006 | CCA licence no.: 643996<br />Data Protection: Z2896846 | Save Britain Money Data Protection: Z671579
        </div>
        </div>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    </body>
</html>