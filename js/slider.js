var slider = (function($, window, document, undefined) {
	return {
		go: function() {
			for(var i in slider.init) {
				slider.init[i]();
			}
		},
		init: {
			setup: function() {
				window.i = 1;
			},
			empty: function() {

			},
			
			first: function() {
                            $("#sliderresulta").on("click", function(ev) {
					ev.preventDefault();
					ev.stopPropagation();
                                        if(window.i == 1) {
                                            window.val = $('#hidden').val();
                                        }
					if(window.i == 2) {
					if(!$("#privacypolicy2").is(":checked")) {
                                            ev.preventDefault();
						ev.stopPropagation();
						alert('Please accept the privacy policy');
                                                return;
					}
                                            
                                           function afterfunctionSlider(data) {
                alert(data);
                $('#overlay').hide();
            }
                                            var form = $('#form1').serialize() + '&slider=true&debtval=' + window.val;
                                            $('#overlay').show();
                                            $('#overlaybox').html('<img src="' + window.baseurl + 'img/loader.gif" />');
                                            debts.utils.center($('#overlaybox'));
                                               debts.utils.ajax(window.baseurl + 'debt-management/find-a-solution', form, afterfunctionSlider);
            
					} else {
					$("#" + window.i).hide();
					window.i++;
					$("#" + window.i).fadeIn(1000).show();
					}
				});
			}
		}
	};
})(jQuery, this, this.document);

jQuery(document).ready(function(e) {
	slider.go();
});