/*jshint eqnull:true */
/*!
 * jQuery Cookie Plugin v1.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2011, Klaus Hartl
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.opensource.org/licenses/GPL-2.0
 */
(function($, document) {

	var pluses = /\+/g;
	function raw(s) {
		return s;
	}
	function decoded(s) {
		return decodeURIComponent(s.replace(pluses, ' '));
	}

	$.cookie = function(key, value, options) {

		// key and at least value given, set cookie...
		if (arguments.length > 1 && (!/Object/.test(Object.prototype.toString.call(value)) || value == null)) {
			options = $.extend({}, $.cookie.defaults, options);

			if (value == null) {
				options.expires = -1;
			}

			if (typeof options.expires === 'number') {
				var days = options.expires, t = options.expires = new Date();
				t.setDate(t.getDate() + days);
			}

			value = String(value);

			return (document.cookie = [
				encodeURIComponent(key), '=', options.raw ? value : encodeURIComponent(value),
				options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
				options.path    ? '; path=' + options.path : '',
				options.domain  ? '; domain=' + options.domain : '',
				options.secure  ? '; secure' : ''
			].join(''));
		}

		// key and possibly options given, get cookie...
		options = value || $.cookie.defaults || {};
		var decode = options.raw ? raw : decoded;
		var cookies = document.cookie.split('; ');
		for (var i = 0, parts; (parts = cookies[i] && cookies[i].split('=')); i++) {
			if (decode(parts.shift()) === key) {
				return decode(parts.join('='));
			}
		}
		return null;
	};

	$.cookie.defaults = {};

})(jQuery, document);
$(function() {
    if($.cookie('debtsreduced') != 'accepted') {
        $('#overlaybox').html('<h5>This site uses cookies</h5><p>The majority of web browsers automatically accept cookies. You can avoid this by changing the settings of your browser.<br />Please be aware that if you stop your browser accepting cookies, some websites may stop working properly on your computer.</p><a href="#" id="cookieaccept">Accept</a> | <a href="' + window.baseurl + 'downloads/useofcookies.pdf" target="_blank">See more information</a>');
        $('#overlay').show();
        debts.utils.center($('#overlaybox'));
    }
    
    $('#cookieaccept').on("click", function() {
        $.cookie('debtsreduced', 'accepted', { expires: 7});
        $('#overlay').hide();
    });
    
    // navigation
    $("ul.nav li").on("mouseenter", function() {
        if($(this).hasClass('down')) {
            clearTimeout(t);
        } else {
            $(this).find("ul.subnav").show().addClass('down');
	}
    }).on("mouseleave", function() {
        $(this).find("ul.subnav").hide();
    });
    
    $('#requestcallback, #requestcallback2').on("click", function() {
        function datafunction(data) {
            $('#overlaybox').html(data);
            debts.utils.center($('#overlaybox'));
            $('#overlay').show();
        }
        debts.utils.ajax(window.baseurl + 'ajax/callback.html', '', datafunction);
    });
});

var debts = (function($, window, document, undefined) {
    return {
       utils: {
           ajax: function(location, data, success, dataType, type) {
               dataType = typeof dataType !== 'undefined' ? dataType : 'html';
               type = typeof type !== 'undefined' ? type : 'POST';
               $.ajax({
                   dataType: dataType,
                   type: type,
                   url: location,
                   data: data,
                   beforeSend: function() {
                       $('#overlay').show();
                   },
                   success: function(data) {
                       success(data);
                   },
                   error: function(xhr, status, error) {
                        alert('XHR: ' + xhr.status + '\nStatus:' + status + '\nError: ' + error);
                        $('#overlay').hide();
                   }
               });
           },
           center: function(obj) {
                obj.css("position","absolute");
                obj.css("top", Math.max(0, (($(window).height() - obj.outerHeight()) / 2) + 
                                                $(window).scrollTop()) + "px");
                obj.css("left", Math.max(0, (($(window).width() - obj.outerWidth()) / 2) + 
                                                $(window).scrollLeft()) + "px");
           }
       } 
    };
})(jQuery, this, this.document);